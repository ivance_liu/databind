/*******homescreen function**********/
function isSupportHomescreen(){
	if("standalone" in navigator){
		return true;
	}
	return false;
}
function isHomescreen(){
	if(isSupportHomescreen() && navigator.standalone){
		return true;
	}
	return false;
}

/* Detect whether device supports orientationchange event
*para:portraitFunc:portrait callback function; landscapeFunc:landscape callback function
demo: var oce=new OrientationChangeExecutor(func1,func2);
*/
function OrientationChangeExecutor(portraitFunc,landscapeFunc){
	var supportsOrientationChange = "onorientationchange" in window;
	
	if(supportsOrientationChange){	
		window.addEventListener('orientationchange', function() {
			if(Math.abs(window.orientation) == 90){
				//alert('landscape');
				$('container_wrapper').focus();
				if(!!landscapeFunc) landscapeFunc();
			} else {
				//alert('portrait');
				$('container_wrapper').focus();
				if(!!portraitFunc) portraitFunc();
			}
		}, false);
	} else {
		//alert('your browser do not support orientationchange');	
	}
}

/***new win***/
function NewWin(url){
	var a = document.createElement('a');
	a.setAttribute("href", url);
	a.setAttribute("target", "_blank");
	var dispatch = document.createEvent("HTMLEvents");
	dispatch.initEvent("click", true, true);
	a.dispatchEvent(dispatch);
	a=null;
}

function changecss(myclass,element,value) {
	if (! document.styleSheets) return
	var CSSRules;  // IE uses rules...
	for (var i = 0; i < document.styleSheets.length; i++) {
		(document.styleSheets[0].cssRules) ? CSSRules = 'cssRules' : CSSRules = 'rules';
		for (var j = 0; j < document.styleSheets[i][CSSRules].length; j++) {
			if (document.styleSheets[i][CSSRules][j].selectorText == myclass) {
				document.styleSheets[i][CSSRules][j].style[element] = value;
			}
		}	
	}
}

