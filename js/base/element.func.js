function getBrowserW(){
	return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;//可见区域宽度
}
function getBrowserH(){
	//alert(document.documentElement.clientHeight);
	return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;//可见区域高度		
}
function getEleW(ele){
	if(typeof ele == 'string'){
		return 	document.getElementById(ele).offsetWidth;
	} else {
		return ele.offsetWidth;	
	}
}
function getEleH(ele){
	if(typeof ele == 'string'){
		return 	document.getElementById(ele).offsetHeight;
	} else {
		return ele.offsetHeight;	
	}
}

/*
window go to top
*/
function goTop(acceleration, time) {
	acceleration = acceleration || 0.1;
	time = time || 16;
 
	var x1 = 0;
	var y1 = 0;
	var x2 = 0;
	var y2 = 0;
	var x3 = 0;
	var y3 = 0;
 
	if (document.documentElement) {
		x1 = document.documentElement.scrollLeft || 0;
		y1 = document.documentElement.scrollTop || 0;
	}
	if (document.body) {
		x2 = document.body.scrollLeft || 0;
		y2 = document.body.scrollTop || 0;
	}
	var x3 = window.scrollX || 0;
	var y3 = window.scrollY || 0;
 
	// 滚动条到页面顶部的水平距离
	var x = Math.max(x1, Math.max(x2, x3));
	// 滚动条到页面顶部的垂直距离
	var y = Math.max(y1, Math.max(y2, y3));
 
	// 滚动距离 = 目前距离 / 速度, 因为距离原来越小, 速度是大于 1 的数, 所以滚动距离会越来越小
	var speed = 1 + acceleration;
	window.scrollTo(Math.floor(x / speed), Math.floor(y / speed));
 
	// 如果距离不为零, 继续调用迭代本函数
	if(x > 0 || y > 0) {
		var invokeFunction = "goTop(" + acceleration + ", " + time + ")";
		window.setTimeout(invokeFunction, time);
	}
}


/*
取得滾動條位置信息,没参数时返回浏览器的滚动条信息
onscroll事件//iPhone滚动停止才触发此事件
getScrollPro();//get window scroll info
setScrollTop();//set window scroll top to 0
getScrollPro(testdiv);//get testdiv scroll info
setScrollTop(0,testdiv);//set testdiv scroll top to 0
*/
function getScrollPro(el){
    var t, l, w, h;
		if(!!el){
			t = el.scrollTop;
	        l = el.scrollLeft;
	        w = el.scrollWidth;
	        h = el.scrollHeight;
		} else {
			if (document.documentElement && document.documentElement.scrollTop) {
				t = document.documentElement.scrollTop;
				l = document.documentElement.scrollLeft;
				w = document.documentElement.scrollWidth;
				h = document.documentElement.scrollHeight;
			} else if (document.body) {
				t = document.body.scrollTop;
				l = document.body.scrollLeft;
				w = document.body.scrollWidth;
				h = document.body.scrollHeight;
			}
		}
    return { t: t, l: l, w: w, h: h };
}
function setScrollTop(v,el){
		if(!!el){
				el.scrollTop=v||0;
		} else {
			if (document.documentElement && document.documentElement.scrollTop) {
				document.documentElement.scrollTop=v||0;
			} else if (document.body) {
				document.body.scrollTop=v||0;
			}
		}
    return;
}

function setScrollLeft(v,el){
		if(!!el){
				el.scrollLeft=v||0;
		} else {
			if (document.documentElement && document.documentElement.scrollLeft) {
				document.documentElement.scrollLeft=v||0;
			} else if (document.body) {
				document.body.scrollLeft=v||0;
			}
		}
    return;
}

//Jquery里面$(document).ready函数的内部实现
document.ready = function (callback) {
    ///兼容FF,Google
    if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', function () {
            document.removeEventListener('DOMContentLoaded', arguments.callee, false);
            callback();
        }, false)
    }
     //兼容IE
    else if (document.attachEvent) {
        document.attachEvent('onreadytstatechange', function () {
              if (document.readyState == "complete") {
                        document.detachEvent("onreadystatechange", arguments.callee);
                        callback();
               }
        })
    }
    else if (document.lastChild == document.body) {
        callback();
    }
}