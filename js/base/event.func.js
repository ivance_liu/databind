/****
trigger given event on element
demo: triggerEvent(document.getElementById('google'),'resize');
note: element not support window object
***/
function triggerEvent(ele,eventType){
	if( document.createEvent) { 
		try{
			var event = document.createEvent ("HTMLEvents"); 
			event.initEvent(eventType, true, true); 
			ele.dispatchEvent(event); 
		} catch(err){}
	} else if(document.createEventObject){ 
		ele.fireEvent("on"+eventType); 
	} 
}


/***
delegate event to container_wrapper dom base on Native
if has baseEle param,please note must use HammerDelegateOff when remove dom
function handle(ev){
	if(ev.target.id=='abc'){
		//do something
	}
}
**/
function nativeDelegateOn(type,handle,baseEle){
	var baseEle=!!baseEle ? baseEle : 'container_wrapper';
	document.getElementById(baseEle).addEventListener(type,handle,false);
}
function nativeDelegateOff(type,handle,baseEle){
	var baseEle=!!baseEle ? baseEle : 'container_wrapper';
	document.getElementById(baseEle).removeEventListener(type,handle,false);
}

/***new win
不能使用javascript來彈出新窗口，要在html上調用才行?如：
<div onclick="newWin('www.baidu.com')">click me</div>
測試可以worb
***/
function newWin(url){
	if(url.indexOf("popsrc=CARE")>-1){

	}else{
		if(url.indexOf("?")>-1){
			url = url+"&popsrc=CARE";
		}else{
			url = url+"?popsrc=CARE";
		}
	}
	//console.log(url);
	//alert(url);
	var a = document.createElement('a');
	a.setAttribute("href", url);
	a.setAttribute("target", "_blank");
	triggerEvent(a,'click');
	a=null;
}

/* Detect whether device supports orientationchange event
*para:portraitFunc:portrait callback function; landscapeFunc:landscape callback function
demo: var oce=new OrientationChangeExecutor(func1,func2);
*/
function OrientationChangeExecutor(portraitFunc,landscapeFunc){
	var supportsOrientationChange = "onorientationchange" in window;
	var orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";
	window.addEventListener(orientationEvent, function() {
		if(!!window.orientation && Math.abs(window.orientation) == 90){
			//alert('landscape');
			$('container_wrapper').focus();
			if(!!landscapeFunc) landscapeFunc();
		} else {
			//alert('portrait');
			$('container_wrapper').focus();
			if(!!portraitFunc) portraitFunc();
		}
	}, false);
}