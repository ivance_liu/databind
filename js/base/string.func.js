/**
* 去除多余空格函数
* trim:去除两边空格 lTrim:去除左空格 rTrim: 去除右空格
* 用法：
*     var str = "   hello ";
*     str = str.trim();
*/
if(!!!String.prototype.trim){
	String.prototype.trim = function(){
		return this.replace(/(^[\\s]*)|([\\s]*$)/g, "");
	}
	String.prototype.lTrim = function(){
		return this.replace(/(^[\\s]*)/g, "");
	}
	String.prototype.rTrim = function(){
		return this.replace(/([\\s]*$)/g, "");
	}
}