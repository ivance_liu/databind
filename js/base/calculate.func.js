/***是否是浮點類型**/
function isFloat(val){
	var regex = /^[+|-]?\d*\.?\d*$/; 
	if (!regex.test(val.toString())){ 
	 return false;     
	}    
	return true;
}
/****精確到小數點後幾位數****/
function floatToFixed(val,n){
	var val=val;
	if(!isFloat(val)){
		val=parseFloat(val);
	}
	try{
		return val.toFixed(n);
	} catch(err){
		var n=Math.pow(10,n);
		return Math.floor(val*n)/n;	
	}
}


/*
 * formatMoney(s,type)
 * 功能：金额按千位逗号分割
 * 参数：s，需要格式化的金额数值.
 * 参数：type,判断格式化后的金额是否需要小数位.
 * 返回：返回格式化后的数值字符串.
 */
function formatMoney(s, type) {
    if (/[^0-9\.]/.test(s)) return "0";
    if (s == null || s == "") return "0";
    s = s.toString().replace(/^(\d*)$/, "$1.");
    s = (s + "00").replace(/(\d*\.\d\d)\d*/, "$1");
    s = s.replace(".", ",");
    var re = /(\d)(\d{3},)/;
    while (re.test(s))
        s = s.replace(re, "$1,$2");
    s = s.replace(/,(\d\d)$/, ".$1");
    if (type == 0) {// 不带小数位(默认是有小数位)
        var a = s.split(".");
        if (a[1] == "00") {
            s = a[0];
        }
    }
    return s;
}

/*
月份補充零
//return string
*/
function monthWithTwoNum(n){
    var n=parseInt(n,10);
    if(n>=0 && n<=9){
        return '0'+n.toString();
    }
    return n.toString();
}