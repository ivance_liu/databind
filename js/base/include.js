/*
遇到的問題，
按序載入問題還沒解決,參考lab.js類庫可以學習代替使用
*/
(function(){
var include={};

/******普通動態 添加css******/
var gAryLinkStyle=[];
function includeLinkStyle(url) {
	if(gAryLinkStyle.toString().indexOf(url)!=-1){
		return;
	}
	var link = document.createElement("link");
	link.rel = "stylesheet";
	link.type = "text/css";
	link.href = url;
	document.getElementsByTagName("head")[0].appendChild(link);
	gAryLinkStyle.push(url);
}
include.loadcss=includeLinkStyle;

/******普通動態 添加js******/
var gAryScriptFile=[];
function includeScriptFile(url,callback){
	if(gAryScriptFile.toString().indexOf(url)!=-1){
		!!callback&&callback();
		return false;
	}
	var script= document.createElement("script");
	script.setAttribute('type', 'text/javascript');
	if(!!callback){
		script.onload = script.onreadystatechange = function(){
			if (script.ready) {
				return false;
			}
			if (!script.readyState || script.readyState == "loaded" || script.readyState == 'complete') {
				script.ready = true;
				callback();
			}
		};
	}
	script.src=url;
	document.getElementsByTagName("head")[0].appendChild(script);
	gAryScriptFile.push(url);
}
include.loadjs=includeScriptFile;

window.Include=include;
	
})();

//console.log(Include.loadcss('base.css'));
//console.log(Include.loadjs('base.js',function(){function1();}));

