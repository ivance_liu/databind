function extend(destination, source) {
    for (var property in source)
      destination[property] = source[property];
    return destination;
}

var DOM=DOM || {};
DOM.querySelectorAll=function(selector){
	return document.querySelectorAll(selector);
}