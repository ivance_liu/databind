//User model class 
var Model={
	items: {},
	extend: function(modelName,attrs) {
		return function(modelId){
			var binder = new DataBinder( modelId ),

			model = {
				attributes: {},

				// The attribute setter publish changes using the DataBinder PubSub
				set: function( attr_name, val ) {
					this.attributes[ attr_name ] = val;
					// Use the `publish` method
					binder.publish( modelId + ":change", attr_name, val, this );
				},

				get: function( attr_name ) {
					return this.attributes[ attr_name ];
				},

				_binder: binder
			};
			
			for (var property in attrs){
				model.set( property, attrs[property] );
				//destination[property] = attrs[property];
			}
			
			// Subscribe to the PubSub
			binder.on( modelId + ":change", function( evt, attr_name, new_val, initiator ) {
				if ( initiator !== model ) {
					model.set( attr_name, new_val );
				}
			});
			Model.items[modelName]=Model.items[modelName] || {};
			Model.items[modelName][modelId]=model;
			return model;
		};
	}
};



/*
function User( uid ) {
  var binder = new DataBinder( uid ),

      user = {
        attributes: {},

        // The attribute setter publish changes using the DataBinder PubSub
        set: function( attr_name, val ) {
          this.attributes[ attr_name ] = val;
          // Use the `publish` method
		  binder.publish( uid + ":change", attr_name, val, this );
        },

        get: function( attr_name ) {
          return this.attributes[ attr_name ];
        },

        _binder: binder
      };
	  

  // Subscribe to the PubSub
  binder.on( uid + ":change", function( evt, attr_name, new_val, initiator ) {
    if ( initiator !== user ) {
      user.set( attr_name, new_val );
    }
  });

  return user;
}

//Title model class 
function Title( s ) {
  var binder = new DataBinder( s ),

      title = {
        attributes: {},

        // The attribute setter publish changes using the DataBinder PubSub
        set: function( attr_name, val ) {
          this.attributes[ attr_name ] = val;
          // Use the `publish` method
		  binder.publish( s + ":change", attr_name, val, this );
        },

        get: function( attr_name ) {
          return this.attributes[ attr_name ];
        },

        _binder: binder
      };
	  

  // Subscribe to the PubSub
  binder.on( s + ":change", function( evt, attr_name, new_val, initiator ) {
    if ( initiator !== title ) {
      title.set( attr_name, new_val );
    }
  });

  return title;
}*/
